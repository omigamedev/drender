package drender.net;

import drender.message.remote.BufferParser;

public class PeerSender extends Thread
{
	/**
	 * Interval beetween flushes in milliseconds.
	 */
	private int interval;
	private BufferParser buffer;
	
	private boolean running;
	private Peer peer;
	
	public PeerSender(int interval, Peer peer)
	{
		super();
		this.interval = interval;
		this.peer = peer;
		running = false;
		buffer = new BufferParser();
	}
	
	@Override
	public void run()
	{
		running = true;
		while(running)
		{
			try
			{
				Thread.sleep(interval);
			}
			catch(InterruptedException e)
			{
			}
			flush();
		}
	}
	
	public void clear()
	{
		buffer.clear();
	}
	
	public void postMessage(byte[] data, int len)
	{
//		System.out.println("post message");
		if(buffer.getRemaining() < len)
			flush();
		buffer.put(data, len);
	}
	
	private void flush()
	{
		if(buffer.hasData())
		{
			// System.out.println("flush");
			peer.sendData(buffer.getBuffer().array(), buffer.getBuffer().position());
			clear();
		}
	}
	
	public BufferParser getBuffer()
	{
		return buffer;
	}

	public int getInterval()
	{
		return interval;
	}

	public void setInterval(int interval)
	{
		this.interval = interval;
	}

	public boolean isRunning()
	{
		return running;
	}

	public void setRunning(boolean running)
	{
		this.running = running;
	}
}
