package drender.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Date;

import drender.message.Message;
import drender.message.MessageConnectionEnd;
import drender.message.MessageQueue;
import drender.message.remote.BufferParser;
import drender.message.remote.MessageRemote;

/**
 * Peer is a thread that handle a TCP connection between a local and a remote
 * socket. When data arrives from the remote socket it is appended to a specific
 * queue that consumes the messages.
 */
public class Peer extends Thread
{
	public static final int TYPE_WORKER = 0x00;
	public static final int TYPE_DAEMON = 0x01;
	 
	public static final int STATE_IDLE = 0x00;
	public static final int STATE_PENDING = 0x01;
	public static final int STATE_RENDERING = 0x02;
	public static final int STATE_DISABLED = 0x03;
	
	private Socket sock;
	private PeerSender bufferedSender;
	private DataOutputStream out;
	private DataInputStream in;
	private MessageQueue mq;
	private boolean connected;
	private int serialNumber;
	private int peerType;
	private int peerState;
	private int failCount;
	private Date taskStartTime;
	private long taskMeanTime;

	public Peer(Socket s, MessageQueue mq) throws Exception
	{
		this.setFailCount(0);
		this.peerState = STATE_IDLE;
		this.sock = s;
		this.out = new DataOutputStream(sock.getOutputStream());
		this.in = new DataInputStream(sock.getInputStream());
		this.mq = mq;
		this.connected = true;
		this.serialNumber = 1;
		bufferedSender = new PeerSender(100, this);
		bufferedSender.start();
		setTaskMeanTime(-1);
	}

	public boolean isConnected()
	{
		return connected;
	}

	public String getIp()
	{
		return sock.getInetAddress().getHostAddress();
	}
	
	public String getHost()
	{
		return sock.getInetAddress().getHostName();
	}

	public void sendData(byte[] data, int len)
	{
		try
		{
			out.write(data, 0, len);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public void sendMessageBuffered(MessageRemote m)
	{
		m.setSerialNumber(serialNumber);
		BufferParser buffer = new BufferParser();
		buffer.putInt(Message.SIGNATURE);
		m.encode(buffer);
		bufferedSender.postMessage(buffer.getBuffer().array(), buffer.getBuffer().position());
		serialNumber++;
	}

	public void sendMessage(MessageRemote m)
	{
		try
		{
			m.setSerialNumber(serialNumber);
			ByteBuffer buffer = MessageRemote.encodeMessage(m);
			out.write(buffer.array(), 0, buffer.position());
			serialNumber++;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public String toString()
	{
		return "Peer [sock=" + sock + "]";
	}

	@Override
	public void run()
	{
		while(isConnected())
		{
			try
			{
				byte[] data = new byte[BufferParser.BUFFER_SIZE];
				int len = in.read(data);

				// if the length of the data read is -1 means that the buffer
				// reader is not valid anymore.
				if(len == -1)
				{
					connected = false;
					break;
				}

				BufferParser buffer = new BufferParser(data, len);
				while(buffer.hasRemaining())
				{
					MessageRemote m = MessageRemote.decodeMessage(buffer);
					if(m == null)
					{
						System.out.println("Received malformed message from " + 
								sock.getRemoteSocketAddress() + ":" + sock.getPort() + 
								" len:" + len + " data:" + new String(data));
						break;
					}
					else
					{
						m.setPeer(this);
						mq.postMessage(m);
					}
				}
			}
			catch(IOException e)
			{
				// e.printStackTrace();
				try
				{
					sock.close();
				}
				catch(IOException e1)
				{
					e1.printStackTrace();
				}
				connected = false;
			}
		}

		// Post the last message to notify the consumer that the connection has
		// been lost.
		Message m = new MessageConnectionEnd(this);
		mq.postMessage(m);
		
		bufferedSender.setRunning(false);

		System.out.println(this + " terminated");
	}

	public int getPeerType()
	{
		return peerType;
	}

	public void setPeerType(int peerType)
	{
		this.peerType = peerType;
	}

	public int getPeerState()
	{
		return peerState;
	}

	public void setPeerState(int peerState)
	{
		this.peerState = peerState;
	}

	public int getFailCount()
	{
		return failCount;
	}

	public void setFailCount(int failCount)
	{
		this.failCount = failCount;
		if(this.failCount > 5)
			setPeerState(STATE_DISABLED);
	}
	
	public void incrementFailCount()
	{
		this.failCount++;
		if(this.failCount > 5)
			setPeerState(STATE_DISABLED);
	}
	
	public void startTaskTimer()
	{
		taskStartTime = new Date();
	}
	
	public void stopTaskTimer()
	{
		long elapsed = getTaskElasped();
		taskMeanTime = taskMeanTime == 0 ? elapsed : (long)Math.floor((taskMeanTime + elapsed) / 2.0f);
		taskStartTime = null;
	}
	
	public long getTaskElasped()
	{
		if(taskStartTime == null)
			return 0;
		Date now = new Date();
		long diff_ms = now.getTime() - taskStartTime.getTime();
		return (long)Math.floor((double)diff_ms / 1000.0);
	}
	
	public long getTaskETA()
	{
		if(taskStartTime == null)
			return 0;
		if(taskMeanTime > 0)
			return taskMeanTime - getTaskElasped();
		return Long.MAX_VALUE;
	}
	
	public Date getTaskStartTime()
	{
		return taskStartTime;
	}

	public void setTaskStartTime(Date taskStartTime)
	{
		this.taskStartTime = taskStartTime;
	}

	public long getTaskMeanTime()
	{
		return taskMeanTime;
	}

	public void setTaskMeanTime(long taskMeanTime)
	{
		this.taskMeanTime = taskMeanTime;
	}
}
