package drender.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Map;

import drender.message.Message;
import drender.message.MessageQueue;
import drender.message.remote.MessageRemote;
import drender.message.remote.MessageRemoteCommand;
import drender.message.remote.MessageRemoteRender;
import drender.message.remote.MessageRemoteString;
import drender.net.Peer;
import drender.server.MainServer;
import drender.utils.StringUtils;
import drender.utils.SystemUtils;
import drender.utils.TaskExecution;

public class MainClient
{
	public static String serverHost = "";

	private MessageQueue mqin;
	private Peer peer;
	private TaskExecution currentTask;

	public static void main(String[] args)
	{
		new MainClient().run(args[0]);
	}

	public void run(String host)
	{
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			@Override
			public void run()
			{
				if(currentTask != null)
					currentTask.destroy();
				
				System.out.println("Goodbye");
			}
		});

		System.out.println("serverHost = " + host);
		serverHost = host;
		
		mqin = new MessageQueue();

		System.out.println("Client started");

		writeSyncCommand();
		connect();

		while(true)
		{
			Message m = mqin.getMessage();

			if(m instanceof MessageRemote)
				System.out.println("Client: " + m);

			switch(m.getType())
			{
			case Message.TYPE_REMOTE_COMMAND:
				MessageRemoteCommand mrrt = (MessageRemoteCommand)m;
				if(mrrt.getCommand().equals("restart"))
				{
					// restart();
				}
				else if(mrrt.getCommand().equals("clear"))
				{
					peer.sendMessage(new MessageRemoteString("images cleared"));
					clearImages();
				}
				else if(mrrt.getCommand().equals("stop"))
				{
//					  if (SystemUtils.isWindows()) 
//					     execute("taskkill", "render");
//					   else
//					     rt.exec("kill -9 " +....);
				}
				else if(mrrt.getCommand().equals("quit"))
				{
					System.exit(0);
				}
				else if(mrrt.getCommand().equals("env"))
				{
					Map<String, String> envmap = SystemUtils.getEnvs();
					for(String envName : envmap.keySet())
						System.out.println(envName + " = " + envmap.get(envName));
				}
				else if(mrrt.getCommand().equals("downsync"))
					execute("lftp", "-f config/downsync.txt", 0, false);
				else if(mrrt.getCommand().equals("upsync"))
					execute("lftp", "-f config/upsync.txt", 0, false);
				else
					execute(mrrt.getCommand(), mrrt.getParams(), 0, false);
				break;
			case Message.TYPE_REMOTE_RENDER:
				MessageRemoteRender mrr = (MessageRemoteRender)m;
				String proj = System.getProperty("user.dir");
				SystemUtils.getEnvs().put("MAYA_PROJECT", proj);
				int start = mrr.getStartFrame();
				int end = mrr.getEndFrame();
				//execute("lftp", "-f config/downsync.txt", mrr.getTaskID(), false).waitProcess();
				String target = proj + SystemUtils.getDirSep() + "scenes"
						+ SystemUtils.getDirSep() + mrr.getFilename(); 
				String[] params = new String[] { 
					"-r mr -proj " + proj,
					"-rd " + proj + SystemUtils.getDirSep() + "images",
					"-s " + start,
					"-e " + end,
					"-v 5",
					target
				};
				
//				long sleep = (long)((Math.random() * 10)+1) % 2 * 2000;
//				SystemUtils.sleep(sleep);
//				peer.sendMessage(new MessageRemoteString("render-complete"));
				
				TaskExecution task = execute("render", StringUtils.join(params, " "), mrr.getTaskID(), false);
				if(task != null)
				{
					peer.sendMessage(new MessageRemoteString("render-start"));
					int fail = task.waitProcess();
					if(fail == 0)
					{
						execute("lftp", "-f config/upsync.txt", 0, false).waitProcess();
						peer.sendMessage(new MessageRemoteString("render-complete"));
					}
					else
					{
						peer.sendMessage(new MessageRemoteString("render-fail"));
					}
				}
				break;
			case Message.TYPE_CONNECTION_END:
				connect();
				break;
			}
		}
	}

	public void writeSyncCommand()
	{
		try
		{
			File config = new File("config");
			if(!config.exists())
				config.mkdir();

			PrintWriter downSync = new PrintWriter("config/downsync.txt");
			downSync.println("open -u drender,drender -p 21 " + serverHost);
			downSync.println("set net:timeout 2");
			downSync.println("set net:max-retries 1");
			downSync.println("set net:reconnect-interval-base 1");
			downSync.println("mirror --exclude-glob images/ --exclude-glob config/  -c /");
			downSync.println("exit");
			downSync.close();

			PrintWriter upSync = new PrintWriter("config/upsync.txt");
			upSync.println("open -u drender,drender -p 21 " + serverHost);
			upSync.println("set net:timeout 2");
			upSync.println("set net:max-retries 1");
			upSync.println("set net:reconnect-interval-base 1");
			upSync.println("mirror --exclude renderData -c -R images/ /images/");
			upSync.println("exit");
			upSync.close();
		}
		catch(FileNotFoundException e)
		{
		}
	}

	private void clearImages()
	{
		File images = new File("images");
		if(images.exists())
			SystemUtils.deleteFolder(images, true);
		else
			images.mkdir();
	}

	public TaskExecution execute(String command, String params, int taskID, boolean storeOutput)
	{
		if(currentTask != null)
			currentTask.destroy();

		TaskExecution task = new TaskExecution();
		boolean success = task.execute(command, params, taskID, peer, true, true, storeOutput);
		currentTask = success ? task : null;
		return currentTask;
	}

	public void connect()
	{
		boolean connected = false;
		System.out.println("Connecting to " + serverHost + "...");
		while(!connected)
		{
			try
			{
				Socket sock = new Socket();
				sock.connect(new InetSocketAddress(serverHost, MainServer.listenPortWorker), 1000);
				peer = new Peer(sock, mqin);
				peer.start();
				connected = true;
			}
			catch(Exception e)
			{
				try
				{
					Thread.sleep(1000);
				}
				catch(InterruptedException e1)
				{
					e1.printStackTrace();
				}
			}
		}
		System.out.println("Connected");
	}

}
