package drender.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;

import drender.message.Message;
import drender.message.MessageConnectionNew;
import drender.message.MessageQueue;
import drender.message.remote.MessageRemote;
import drender.message.remote.MessageRemoteCommand;
import drender.message.remote.MessageRemoteString;
import drender.net.Peer;
import drender.server.MainServer;
import drender.utils.SystemUtils;
import drender.utils.TaskExecution;

public class MainDaemon
{
	public static String serverHost = "";

	private MessageQueue mqin;
	private Peer peer;
	private ArrayList<TaskExecution> workers;
	private HashSet<InetAddress> listOfBroadcasts;
	private ArrayList<String> listHosts;
	private TaskExecution currentTask;

	public static void main(String[] args)
	{
		new MainDaemon().run();
	}

	public void run()
	{
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			@Override
			public void run()
			{
				if(currentTask != null)
					currentTask.destroy();
				for(TaskExecution task : workers)
					task.getProcess().destroy();
				System.out.println("Goodbye");
			}
		});

		mqin = new MessageQueue();
		workers = new ArrayList<TaskExecution>();

		System.out.println("Client started");

		checkBroadcasts();
		connect();
		writeSyncCommand();
		execute("lftp", "-f config/daemon-downsync.txt", 0, true, true).waitProcess();
		if(currentTask.getErrBuffer().indexOf("MainDaemon.class") != -1 ||
				currentTask.getOutBuffer().indexOf("MainDaemon.class") != -1)
		{
			System.out.println("RESTARTING");
			restart();
		}
		else
		{
			System.out.println("The system is UpToDate");
		}
		
		System.out.print("Checking Maya Version... ");
		execute("maya", "-v", 0, true, true).waitProcess();
		if(SystemUtils.isMac())
		{
			System.out.print("Preloading Maya for Mac systems... ");
			execute("maya", "-help", 0, false, false).waitProcess();
			System.out.println("done.");
		}

		while(true)
		{
			Message m = mqin.getMessage();

			if(m instanceof MessageRemote)
				System.out.println("Client: " + m);

			switch(m.getType())
			{
			case Message.TYPE_REMOTE_COMMAND:
				MessageRemoteCommand mrr = (MessageRemoteCommand) m;
				if(mrr.getCommand().equals("restart"))
				{
//					for(TaskExecution task : workers)
//						task.getProcess().destroy();
//					startNode();
					
					restart();
				}
				else if(mrr.getCommand().equals("upload"))
				{
					execute("lftp", "-f config/upsync.txt", 0, true, false);
				}
				else if(mrr.getCommand().equals("add-node"))
				{
					startNode();
				}
				else if(mrr.getCommand().equals("quit"))
				{
					System.exit(0);
				}
				else if(mrr.getCommand().equals("kill"))
				{
					if(currentTask != null)
					{
						currentTask.destroy();
						System.out.println("process killed");
						currentTask = null;
					}
					SystemUtils.kill("MayaBatch" + SystemUtils.getExeExtension());
				}
				else if(mrr.getCommand().equals("update"))
				{
					writeSyncCommand();
					int status = execute("lftp", "-f config/daemon-downsync.txt", 0, true, false).waitProcess();
//					System.out.println("Update " + (status == 0 ? "success" : "fail"));
					peer.sendMessage(new MessageRemoteString("update-" + (status == 0 ? "success" : "fail")));
				}
				break;
			case Message.TYPE_CONNECTION_NEW:
				startNode();
				break;
			case Message.TYPE_CONNECTION_END:
				if(currentTask != null)
				{
					currentTask.destroy();
					System.out.println("process killed");
				}
				synchronized(workers)
				{
					for(TaskExecution task : workers)
						task.getProcess().destroy();					
				}
				connect();
				break;
			}
		}
	}

	private void restart()
	{
		String bin = System.getProperty("user.dir") + SystemUtils.getDirSep();
		execute("lftp", "-f config/daemon-downsync.txt", 0, true, false).waitProcess();
		// execute("java", "-cp " + bin + " drender.client.MainDaemon", 0,
		// false);
		
		if(SystemUtils.isWindows())
		{
			execute("cmd", "/c start /max java -cp " + bin + " drender.client.MainDaemon", 0, false, false);
		}
		else if(SystemUtils.isMac())
		{
			// String term = "/Applications/Utilities/Terminal.app/";
			// String java = SystemUtils.findPath("java");
			// execute("open", term + " " + java + " --args -cp " + bin +
			// " drender.client.MainDaemon", 0, false);
			execute("java", "-cp " + bin + " drender.client.MainDaemon", 0, false, false);
		}
		else
		{
			System.out.println("restart unsupported");
		}
		
		if(currentTask != null)
		{
			peer.sendMessage(new MessageRemoteString("restart-ok"));
			currentTask = null;
			System.exit(0);
		}
		else 
			peer.sendMessage(new MessageRemoteString("restart-fail"));
	}

	private void checkBroadcasts()
	{
		listOfBroadcasts = new HashSet<InetAddress>();
		listHosts = new ArrayList<String>();
		try
		{
//			listOfBroadcasts.add(InetAddress.getByName("MACBOOKAIR-A284"));
//			listOfBroadcasts.add(InetAddress.getByName("MACBOOKAIR-A284.local"));
			listHosts.add("192.168.1.255");
			listHosts.add("192.168.1.110");
			listHosts.add("192.168.0.255");
			listHosts.add("192.168.127.172");
			if(SystemUtils.isMac())
			{
				listHosts.add("MacBook-Air-di-Omar.local");
				listHosts.add("SUPERGAMING.local");				
				listHosts.add("OMIMSI.local");				
				listHosts.add("ASUS.local");				
			}
			else
			{
				listHosts.add("MACBOOKAIR-A284");
				listHosts.add("SUPERGAMING");
				listHosts.add("OMIMSI");
				listHosts.add("ASUS");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		Enumeration<NetworkInterface> list;
		try
		{
			list = NetworkInterface.getNetworkInterfaces();

			while(list.hasMoreElements())
			{
				NetworkInterface iface = (NetworkInterface) list.nextElement();

				if(iface == null)
					continue;

				if(!iface.isLoopback() && iface.isUp())
				{
//					System.out.println("Found non-loopback, up interface:" + iface);
					Iterator<InterfaceAddress> it = iface.getInterfaceAddresses().iterator();
					while(it.hasNext())
					{
						InterfaceAddress address = (InterfaceAddress) it.next();
						// System.out.println("Found address: " + address);
						if(address == null)
							continue;
						InetAddress broadcast = address.getBroadcast();
						if(broadcast != null && !broadcast.getHostAddress().toString().equals("0.0.0.0"))
						{
							System.out.println("Found broadcast: " + broadcast);
							listOfBroadcasts.add(broadcast);
							listHosts.add(broadcast.getHostAddress());
						}
					}
				}
			}
		}
		catch(SocketException ex)
		{
			System.err.println("Error while getting network interfaces");
			ex.printStackTrace();
		}
	}

	private void startNode()
	{
		String bin = System.getProperty("user.dir") + SystemUtils.getDirSep();
		execute("lftp", "-f config/daemon-downsync.txt", 0, true, false).waitProcess();
		final TaskExecution worker = execute("java", "-cp " + bin + " drender.client.MainClient " + serverHost, 0, false, false);
		if(worker != null)
		{
			workers.add(worker);
			new Thread()
			{
				public void run() 
				{
					try
					{
						worker.getProcess().waitFor();
					}
					catch(InterruptedException e)
					{
						e.printStackTrace();
					}
					synchronized(workers)
					{
						workers.remove(worker);
						System.out.println("Worker dead");						
					}
				};
			}.start();
		}
		currentTask = null;
	}

	public void writeSyncCommand()
	{
		try
		{
			File config = new File("config");
			if(!config.exists())
				config.mkdir();

			PrintWriter downSync = new PrintWriter("config/daemon-downsync.txt");
			downSync.println("open -u drender,drender -p 21 " + serverHost);
			downSync.println("set net:timeout 2");
			downSync.println("set net:max-retries 1");
			downSync.println("set net:reconnect-interval-base 1");
			downSync.println("mirror -v --exclude-glob images/ -c /");
			downSync.println("exit");
			downSync.close();
		}
		catch(FileNotFoundException e)
		{
		}
	}

	public TaskExecution execute(String command, String params, int taskID, boolean sendStream, boolean storeOutput)
	{
			if(currentTask != null)
				currentTask.destroy();

			TaskExecution task = new TaskExecution();
			boolean success = task.execute(command, params, taskID, sendStream ? peer : null, true, true, storeOutput);
			currentTask = success ? task : null;
			return currentTask;
	}

	public void connect()
	{
		boolean connected = false;

		for(String addr : listHosts)
			System.out.println("Trying to connect to " + addr + "...");

		while(!connected)
		{
			for(String addr : listHosts)
			{
				try
				{
					Socket sock = new Socket();
					sock.connect(new InetSocketAddress(addr, MainServer.listenPortDaemon), 1000);
					peer = new Peer(sock, mqin);
					peer.start();
					connected = true;
					mqin.postMessage(new MessageConnectionNew(sock, MainServer.listenPortDaemon));
					break;
				}
				catch(Exception e)
				{
					try
					{
						Thread.sleep(200);
					}
					catch(InterruptedException e1)
					{
						e1.printStackTrace();
					}
				}
			}
		}
		System.out.println("Connected to " + peer.getHost());
		serverHost = peer.getHost();
	}
}
