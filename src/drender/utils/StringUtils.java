package drender.utils;

import java.util.ArrayList;

public class StringUtils
{
	static private StringUtils singleton;

	private static ArrayList<String> exclude_begins;
	private static ArrayList<String> exclude_contains;
	private static ArrayList<String> include;

	private StringUtils()
	{
		exclude_begins = new ArrayList<String>();
		exclude_begins.add("// ");
		exclude_begins.add("mental ray");
		exclude_begins.add("Prog:");
		exclude_begins.add("MEM ");
		exclude_begins.add("MSG ");
		exclude_begins.add("Info:");
		exclude_begins.add("Warning:");
		exclude_begins.add("SCEN ");
		exclude_begins.add("RC ");
		exclude_begins.add("RCI ");
		exclude_begins.add("PHEN ");
		exclude_begins.add("JOB ");
		exclude_begins.add("IMG ");
		exclude_begins.add("GAPM ");
		exclude_begins.add("Loading GoZ");

		exclude_contains = new ArrayList<String>();
		exclude_contains.add("find_fast_cwd:");
		exclude_contains.add("cygwin@cygwin.com");

		include = new ArrayList<String>();
		include.add("writing frame buffer");
	}

	private boolean match(String str)
	{
		boolean print = true;

		if(str.trim().length() == 0)
			print = false;

		for(String s : exclude_begins)
		{
			if(str.startsWith(s))
			{
				print = false;
				break;
			}
		}

		for(String s : exclude_contains)
		{
			if(str.contains(s))
			{
				print = false;
				break;
			}
		}

		if(print == false)
		{
			for(String s : include)
			{
				if(str.contains(s))
				{
					print = true;
					break;
				}
			}
		}

		return print;
	}

	public static boolean isPrintable(String s)
	{
		if(singleton == null)
			singleton = new StringUtils();
		return singleton.match(s);
	}

	public static String join(String[] s, String glue)
	{
		int k = s.length;
		if(k == 0)
			return null;
		StringBuilder out = new StringBuilder();
		out.append(s[0]);
		for(int x = 1; x < k; ++x)
			out.append(glue).append(s[x]);
		return out.toString();
	}

}
