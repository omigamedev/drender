package drender.utils;

import drender.message.remote.MessageRemoteString;
import drender.net.Peer;

public class ProcessWaiter extends Thread
{
	private Process process;
	private String name;
	private Peer peer;
	private String notifySuccess;
	private String notifyFail;
	private int result;

	public ProcessWaiter(Process process, String name)
	{
		super();
		this.setProcess(process);
		this.name = name;
	}

	public ProcessWaiter(Process process, String name, Peer peer, String notifySuccess, String notifyFail)
	{
		super();
		this.setProcess(process);
		this.name = name;
		this.notifyFail = notifyFail;
		this.notifySuccess = notifySuccess;
	}

	@Override
	public void run()
	{
		result = -1;
		try
		{
			result = process.waitFor();
			if(peer != null)
			{
				if(result == 0 && notifySuccess != null)
					peer.sendMessage(new MessageRemoteString(notifySuccess));
				if(result != 0 && notifyFail != null)
					peer.sendMessage(new MessageRemoteString(notifyFail));
			}
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		System.out.println("Process " + name + " ended with result: " + result);
	}

	public Process getProcess()
	{
		return process;
	}

	public void setProcess(Process process)
	{
		this.process = process;
	}
}
