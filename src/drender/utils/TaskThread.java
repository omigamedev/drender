package drender.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import drender.message.remote.MessageRemoteTaskOutput;
import drender.net.Peer;

public class TaskThread extends Thread
{
	public static final int STREAM_OUT = 0x00;
	public static final int STREAM_ERR = 0x01;

	private Process proc;
	private Peer peer;
	private StringBuffer outBuffer;
	private StringBuffer errBuffer;
	private int streamType;
	private int taskID;

	public TaskThread(Process proc, int streamType, Peer peer, int tastID, StringBuffer outBuffer, StringBuffer errBuffer)
	{
		super();
		this.proc = proc;
		this.peer = peer;
		this.streamType = streamType;
		this.taskID = tastID;
		this.outBuffer = outBuffer;
		this.errBuffer = errBuffer;
	}

	@Override
	public void run()
	{
		try
		{
			if(streamType == STREAM_OUT)
				scanOutput();
			else if(streamType == STREAM_ERR)
				scanError();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("thread end");
	}

	private void scanOutput()
	{
		final InputStream inStream = proc.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
		String line;
		try
		{
			while((line = reader.readLine()) != null)
			{
				System.out.println("OutStream: " + line);
				if(peer != null && StringUtils.isPrintable(line))
					peer.sendMessageBuffered(new MessageRemoteTaskOutput(taskID, streamType, line));
				if(outBuffer != null)
					outBuffer.append(line + "\n");
			}
			reader.close();
		}
		catch(IOException e)
		{
			System.out.println("OutStream closed");
		}
	}

	private void scanError()
	{
		final InputStream inStream = proc.getErrorStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
		String line;
		try
		{
			while((line = reader.readLine()) != null)
			{
				System.out.println("ErrStream: " + line);
				if(peer != null && StringUtils.isPrintable(line))
					peer.sendMessageBuffered(new MessageRemoteTaskOutput(taskID, streamType, line));
				if(errBuffer != null)
					errBuffer.append(line + "\n");
			}
			reader.close();
		}
		catch(IOException e)
		{
			System.out.println("ErrStream closed");
		}
	}
}
