package drender.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class SystemUtils
{
	public static final String OS_WINDOWS = "win";
	public static final String OS_OSX = "osx";
	public static final String OS_UNIX = "unix";
	public static final String OS_SOLARIS = "solaris";

	private static HashMap<String, String> env = null;

	private static String os = null;
	private static String dirSep;
	private static String envSep;
	private static String exeExtension;

	public static String getOS()
	{
		if(os == null)
		{
			os = System.getProperty("os.name");
			System.out.println(os);

			if(isMac())
			{
				dirSep = "/";
				envSep = ":";
				exeExtension = "";
				addPath("/Applications/Autodesk/maya2015/Maya.app/Contents/bin/");
//				addPath("/Applications/Utilities/Terminal.app/Contents/MacOS/");
			}
			else if(SystemUtils.isWindows())
			{
				envSep = ";";
				dirSep = "\\";
				exeExtension = ".exe";
				addPath("C:\\Program Files\\Autodesk\\Maya2015\\bin\\");
			}
		}
		return os;
	}
	
	public static void kill(String image)
	{
		TaskExecution killer = new TaskExecution();
		if(isWindows())
			killer.execute("taskkill", "/F /IM " + image, 0, null, false, false, false);
		else
			killer.execute("killall", "-9 " + image, 0, null, false, false, false);
	}

	public static void sleep(long ms)
	{
		try
		{
			Thread.sleep(ms);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public static HashMap<String, String> getEnvs()
	{
		if(env == null)
		{
			Map<String, String> envmap = System.getenv();
			env = new HashMap<String, String>();
			for(String envName : envmap.keySet())
			{
				env.put(envName.toUpperCase(), envmap.get(envName));
			}
		}
		return env;
	}

	public static String[] getEnvsArray()
	{
		HashMap<String, String> envmap = getEnvs();
		String[] ret = new String[envmap.size()];
		int i = 0;
		for(String envName : envmap.keySet())
			ret[i++] = envName + "=" + envmap.get(envName);
		return ret;
	}

	public static void addPath(String path)
	{
		HashMap<String, String> env = getEnvs();
		env.put("PATH", env.get("PATH") + SystemUtils.getEnvSep() + path);
	}

	public static String findPath(String command)
	{
		String env = getEnvs().get("PATH");
		String[] dirs = env.split(SystemUtils.getEnvSep());
		for(String d : dirs)
		{
			File f = new File(d + SystemUtils.getDirSep() + command + SystemUtils.getExeExtension());
			// System.out.println("test: " + f.getAbsolutePath());
			if(f.exists())
				return f.getAbsolutePath();
		}
		return null;
	}

	public static void deleteFolder(File folder, boolean root)
	{
		File[] files = folder.listFiles();
		if(files != null)
		{
			for(File f : files)
			{
				if(f.isDirectory())
				{
					deleteFolder(f, false);
				}
				else
				{
					f.delete();
				}
			}
		}
		if(!root)
			folder.delete();
	}

	public static boolean isWindows()
	{
		return (getOS().indexOf("Win") >= 0);
	}

	public static boolean isMac()
	{
		return (getOS().indexOf("Mac") >= 0);
	}

	public static boolean isUnix()
	{
		return (getOS().indexOf("nix") >= 0 || getOS().indexOf("nux") >= 0 || getOS().indexOf("aix") > 0);
	}

	public static boolean isSolaris()
	{
		return (getOS().indexOf("sunos") >= 0);
	}

	public static String getDirSep()
	{
		getOS();
		return dirSep;
	}

	public static String getEnvSep()
	{
		getOS();
		return envSep;
	}

	public static String getExeExtension()
	{
		getOS();
		return exeExtension;
	}
}
