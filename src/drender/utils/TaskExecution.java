package drender.utils;

import java.io.IOException;

import drender.net.Peer;

public class TaskExecution
{
	private TaskThread outStream;
	private TaskThread errStream;
	private StringBuffer outBuffer;
	private StringBuffer errBuffer;
	private Process process;
	private ProcessWaiter waiter;

	public TaskExecution()
	{
		super();
	}

	public boolean execute(String command, String params, int taskID, Peer peer, 
			boolean withStream, boolean withWaiter, boolean storeOutput)
	{
		try
		{
			String cmd = SystemUtils.findPath(command);
			if(cmd != null)
			{
				System.out.println("starting " + cmd + " " + params);
				process = Runtime.getRuntime().exec(cmd + " " + params, SystemUtils.getEnvsArray());
				if(process == null)
					return false;
				outBuffer = storeOutput ? new StringBuffer() : null;
				errBuffer = storeOutput ? new StringBuffer() : null;
				if(withStream)
				{
					outStream = new TaskThread(process, TaskThread.STREAM_OUT, peer, taskID, outBuffer, errBuffer);
					errStream = new TaskThread(process, TaskThread.STREAM_ERR, peer, taskID, outBuffer, errBuffer);
					outStream.start();
					errStream.start();
				}
				if(withWaiter)
				{
					ProcessWaiter waiter = new ProcessWaiter(process, command);
					waiter.start();
				}
				return true;
			}
			else
			{
				System.out.println(command + " not found");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return false;
	}


	public void destroy()
	{
		if(process != null)
		{
			process.destroy();
		}
	}

	public int waitProcess()
	{
		try
		{
			return process.waitFor();
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		return -1;
	}

	public TaskThread getOutStream()
	{
		return outStream;
	}

	public void setOutStream(TaskThread outStream)
	{
		this.outStream = outStream;
	}

	public TaskThread getErrStream()
	{
		return errStream;
	}

	public void setErrStream(TaskThread errStream)
	{
		this.errStream = errStream;
	}

	public Process getProcess()
	{
		return process;
	}

	public void setProcess(Process process)
	{
		this.process = process;
	}

	public ProcessWaiter getWaiter()
	{
		return waiter;
	}

	public void setWaiter(ProcessWaiter waiter)
	{
		this.waiter = waiter;
	}

	public StringBuffer getOutBuffer()
	{
		return outBuffer;
	}

	public StringBuffer getErrBuffer()
	{
		return errBuffer;
	}

}
