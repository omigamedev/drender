package drender.message;

public class MessageCommand extends Message
{
	private String command;
	private String params;
	
	public MessageCommand(String command, String params)
	{
		this.type = TYPE_COMMAND;
		this.command = command;
		this.params = params;
	}

	public String getCommand()
	{
		return command;
	}

	public void setCommand(String command)
	{
		this.command = command;
	}

	@Override
	public String toString()
	{
		return "MessageCommand [type=" + type + ", command=" + command + ", params=" + params + "]";
	}

	public String getParams()
	{
		return params;
	}

	public void setParams(String params)
	{
		this.params = params;
	}

}
