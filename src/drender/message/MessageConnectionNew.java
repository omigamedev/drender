package drender.message;

import java.net.Socket;

public class MessageConnectionNew extends Message
{
	private Socket sock;
	private int listenPort;

	public MessageConnectionNew(Socket sock, int listenPort)
	{
		super();
		this.type = TYPE_CONNECTION_NEW;
		this.sock = sock;
		this.listenPort = listenPort;
	}

	public Socket getSocket()
	{
		return sock;
	}

	@Override
	public String toString()
	{
		return "MessageConnectionNew [sock=" + sock + ", listenPort=" + listenPort + "]";
	}

	@Override
	public String getTypeString()
	{
		return "NEW_CONNECTION";
	}

	public int getListenPort()
	{
		return listenPort;
	}

	public void setListenPort(int listenPort)
	{
		this.listenPort = listenPort;
	}
}
