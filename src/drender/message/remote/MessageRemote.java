package drender.message.remote;

import java.nio.ByteBuffer;

import drender.message.Message;
import drender.net.Peer;

public class MessageRemote extends Message
{
	protected int serialNumber;
	protected Peer peer;

	public MessageRemote()
	{
		this.type = TYPE_REMOTE;
		this.serialNumber = 0;
	}

	public MessageRemote(int serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	public void encode(BufferParser buffer)
	{
		buffer.putInt(serialNumber);
		buffer.putInt(type);
	}

	public int messageSize()
	{
		return Integer.SIZE * 2;
	}

	public Peer getPeer()
	{
		return peer;
	}

	public void setPeer(Peer peer)
	{
		this.peer = peer;
	}

	@Override
	public String getTypeString()
	{
		return "REMOTE";
	}

	public int getSerialNumber()
	{
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	@Override
	public String toString()
	{
		return "MessageRemote [serialNumber=" + serialNumber + "]";
	}

	/**
	 * Decode a binary message from the network.
	 * 
	 * @param data
	 *            The binary data comes from the socket.
	 * @return The decoded MessageRemote to be casted in some of its subclass.
	 */
	public static MessageRemote decodeMessage(BufferParser buffer)
	{
		int signature = buffer.getInt();
		if(signature == Message.SIGNATURE) // test if the message is valid
		{
			int serialNumber = buffer.getInt();
			int type = buffer.getInt();

			switch(type)
			{
			case Message.TYPE_REMOTE:
				return new MessageRemote(serialNumber);
			case Message.TYPE_REMOTE_COMMAND:
				return new MessageRemoteCommand(serialNumber, buffer);
			case Message.TYPE_REMOTE_RENDER:
				return new MessageRemoteRender(serialNumber, buffer);
			case Message.TYPE_REMOTE_TASKOUTPUT:
				return new MessageRemoteTaskOutput(serialNumber, buffer);
			case Message.TYPE_REMOTE_STRING:
				return new MessageRemoteString(serialNumber, buffer);
			}
		}
		return null;
	}

	/**
	 * Encode the message in a binary format.
	 * 
	 * @param m
	 *            The message to be encoded.
	 * @return The ByteBuffer containing the binary data.
	 */
	public static ByteBuffer encodeMessage(MessageRemote m)
	{
		BufferParser buffer = new BufferParser();

		buffer.putInt(Message.SIGNATURE);
		m.encode(buffer);

		return buffer.getBuffer();
	}
}
