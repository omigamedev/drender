package drender.message.remote;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class BufferParser
{
	public static final int BUFFER_SIZE = 16384;
	
	private ByteBuffer buffer;
	private int size;

	public int getSize()
	{
		return size;
	}

	public BufferParser(byte[] data, int size)
	{
		buffer = ByteBuffer.wrap(data);
		this.size = size;
	}

	public BufferParser()
	{
		buffer = ByteBuffer.allocate(BUFFER_SIZE);
	}
	
	public void clear()
	{
		buffer.clear();
	}
	
	public boolean hasRemaining()
	{
		return buffer.position() < size;
	}
	
	public int getRemaining()
	{
		return size - buffer.position();
	}
	
	public boolean hasData()
	{
		return buffer.position() > 0;
	}

	public ByteBuffer getBuffer()
	{
		return buffer;
	}
	
	public void put(byte[] data, int len)
	{
		buffer.put(data, 0, len);
	}

	public int getInt()
	{
		return buffer.getInt();
	}

	public void putInt(int value)
	{
		buffer.putInt(value);
	}

	public String getString()
	{
		int textSize = buffer.getInt();
		byte[] textData = new byte[textSize];
		buffer.get(textData, 0, textSize);
		try
		{
			return new String(textData, "UTF-8");
		}
		catch(UnsupportedEncodingException e)
		{
			return null;
		}
	}

	public void putString(String s)
	{
		byte[] data = null;
		try
		{
			data = s.getBytes("UTF-8");
		}
		catch(UnsupportedEncodingException e)
		{
			return;
		}
		buffer.putInt(data.length);
		buffer.put(data);
	}
}
