package drender.message.remote;

public class MessageRemoteCommand extends MessageRemote
{
	private String command = null;
	private String params = null;

	public MessageRemoteCommand(String command, String params)
	{
		this.type = TYPE_REMOTE_COMMAND;
		this.command = command;
		this.params = params;
	}
	
	public MessageRemoteCommand(int serialNumber, BufferParser buffer)
	{
		this.serialNumber = serialNumber; 
		this.type = TYPE_REMOTE_COMMAND;
		this.command = buffer.getString();
		this.params = buffer.getString();
	}
	
	@Override
	public void encode(BufferParser buffer)
	{
		super.encode(buffer);
		buffer.putString(command);
		buffer.putString(params);
	}

	public String getCommand()
	{
		return command;
	}

	public void setCommand(String command)
	{
		this.command = command;
	}

	public String getParams()
	{
		return params;
	}

	public void setParams(String params)
	{
		this.params = params;
	}

	@Override
	public String toString()
	{
		return "MessageRemoteCommand [host=" + peer.getHost() + ", command=" + command + ", params=" + params + "]";
	}

}
