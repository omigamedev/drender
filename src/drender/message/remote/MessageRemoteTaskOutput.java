package drender.message.remote;

public class MessageRemoteTaskOutput extends MessageRemote
{
	private int taskID;
	private int streamType;
	private String streamText;

	public MessageRemoteTaskOutput(int taskID, int streamType, String streamText)
	{
		type = TYPE_REMOTE_TASKOUTPUT;
		this.setStreamText(streamText);
		this.setStreamType(streamType);
		this.setTaskID(taskID);
	}

	public MessageRemoteTaskOutput(int serialNumber, BufferParser buffer)
	{
		this.serialNumber = serialNumber;
		this.type = TYPE_REMOTE_TASKOUTPUT;
		setTaskID(buffer.getInt());
		streamType = buffer.getInt();
		streamText = buffer.getString();
	}

	@Override
	public void encode(BufferParser buffer)
	{
		super.encode(buffer);
		buffer.putInt(getTaskID());
		buffer.putInt(streamType);
		buffer.putString(streamText);
	}

	public int getStreamType()
	{
		return streamType;
	}

	public void setStreamType(int streamType)
	{
		this.streamType = streamType;
	}

	public String getStreamText()
	{
		return streamText;
	}

	public void setStreamText(String streamText)
	{
		this.streamText = streamText;
	}

	@Override
	public String toString()
	{
		return "MessageRemoteTaskOutput [host=" + peer.getHost() + ", taskID=" + getTaskID() + ", streamType=" + streamType + ", streamText=" + streamText + "]";
	}

	public int getTaskID()
	{
		return taskID;
	}

	public void setTaskID(int taskID)
	{
		this.taskID = taskID;
	}
}
