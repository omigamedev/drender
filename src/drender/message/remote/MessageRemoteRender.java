package drender.message.remote;

public class MessageRemoteRender extends MessageRemote
{
	private String filename;
	private int startFrame;
	private int endFrame;
	private int taskID;

	public MessageRemoteRender(String filename, int startFrame, int framesCount, int taskID)
	{
		super();
		this.type = TYPE_REMOTE_RENDER;
		this.filename = filename;
		this.startFrame = startFrame;
		this.endFrame = framesCount;
		this.setTaskID(taskID);
	}

	public MessageRemoteRender(int serialNumber, BufferParser buffer)
	{
		this.serialNumber = serialNumber;
		this.type = TYPE_REMOTE_RENDER;
		this.filename = buffer.getString();
		this.startFrame = buffer.getInt();
		this.endFrame = buffer.getInt();
		this.setTaskID(buffer.getInt());
	}

	@Override
	public void encode(BufferParser buffer)
	{
		super.encode(buffer);
		buffer.putString(filename);
		buffer.putInt(startFrame);
		buffer.putInt(endFrame);
		buffer.putInt(getTaskID());
	}

	public String getFilename()
	{
		return filename;
	}

	public void setFilename(String filename)
	{
		this.filename = filename;
	}

	public int getStartFrame()
	{
		return startFrame;
	}

	public void setStartFrame(int startFrame)
	{
		this.startFrame = startFrame;
	}

	public int getEndFrame()
	{
		return endFrame;
	}

	public void setEndFrame(int endFrame)
	{
		this.endFrame = endFrame;
	}

	@Override
	public String toString()
	{
		return "MessageRemoteRender [host=" + peer.getHost() + ", filename=" + filename + ", startFrame=" + startFrame + ", endFrame=" + endFrame + ", taskID=" + taskID + "]";
	}

	public int getTaskID()
	{
		return taskID;
	}

	public void setTaskID(int taskID)
	{
		this.taskID = taskID;
	}
}
