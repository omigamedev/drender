package drender.message.remote;

public class MessageRemoteString extends MessageRemote
{
	private String string;
	
	public MessageRemoteString(String string)
	{
		this.type = TYPE_REMOTE_STRING;
		this.setString(string);
	}
	
	public MessageRemoteString(int serialNumber, BufferParser buffer)
	{
		this.serialNumber = serialNumber; 
		this.type = TYPE_REMOTE_STRING;
		this.string = buffer.getString();
	}
	
	@Override
	public void encode(BufferParser buffer)
	{
		super.encode(buffer);
		buffer.putString(string);
	}

	public String getString()
	{
		return string;
	}

	public void setString(String string)
	{
		this.string = string;
	}

	@Override
	public String toString()
	{
		return "MessageRemoteString [host=" + peer.getHost() + ", string=" + string + "]";
	}
}
