package drender.message;

public class Message
{
	/**
	 * The signature indicates that this message is a valid message.
	 */
	public static final int SIGNATURE = 0xDEADBEEF;

	/*
	 * Types of messages.
	 */
	public static final int TYPE_BASE = 0x000;
	public static final int TYPE_CONNECTION_NEW = 0x001;
	public static final int TYPE_CONNECTION_END = 0x002;
	public static final int TYPE_COMMAND = 0x003;

	/*
	 * Types of messages that travels in the network
	 */
	public static final int TYPE_REMOTE = 0x100;
	public static final int TYPE_REMOTE_COMMAND = 0x101;
	public static final int TYPE_REMOTE_RENDER = 0x102;
	public static final int TYPE_REMOTE_TASKOUTPUT = 0x103;
	public static final int TYPE_REMOTE_STRING = 0x104;

	// Indicates the type of the message based on the types specified above.
	protected int type;

	public Message()
	{
		super();
		this.type = TYPE_BASE;
	}

	public int getType()
	{
		return type;
	}

	public String getTypeString()
	{
		return "TYPE_BASE";
	}

	@Override
	public String toString()
	{
		return "Message [type=" + getTypeString() + "]";
	}
}
