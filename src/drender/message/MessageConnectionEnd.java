package drender.message;

import drender.net.Peer;

public class MessageConnectionEnd extends Message
{
	private Peer peer;

	public MessageConnectionEnd(Peer peer)
	{
		super();
		this.type = TYPE_CONNECTION_END;
		this.peer = peer;
	}

	public Peer getPeer()
	{
		return peer;
	}

	@Override
	public String toString()
	{
		return "MessageConnectionEnd [peer=" + peer + ", type=" + type + "]";
	}
}
