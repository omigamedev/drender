package drender.message;

import java.util.LinkedList;
import java.util.Queue;

public class MessageQueue
{
	private Queue<Message> messages;
	private Object lock;

	public MessageQueue()
	{
		messages = new LinkedList<Message>();
		lock = new Object();
	}

	public void cleanUp()
	{
		messages.clear();
	}

	public boolean isEmpty()
	{
		return messages.size() == 0;
	}

	public void postMessage(Message msg)
	{
		synchronized(lock)
		{
			messages.add(msg);
			lock.notifyAll();
		}
	}

	public Message getMessage()
	{
		Message ret = null;
		synchronized(lock)
		{
			if(messages.isEmpty())
			{
				try
				{
					lock.wait();
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
					return null;
				}
			}
			ret = messages.poll();
		}
		return ret;
	}

}
