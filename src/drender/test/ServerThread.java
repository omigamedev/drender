package drender.test;

import drender.server.MainServer;

public class ServerThread extends Thread
{
	@Override
	public void run()
	{
		new MainServer().run();
	}
}
