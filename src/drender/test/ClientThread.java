package drender.test;

import drender.client.MainClient;

public class ClientThread extends Thread
{
	@Override
	public void run()
	{
		new MainClient().run("localhost");
	}
}
