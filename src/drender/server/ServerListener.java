package drender.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import drender.message.Message;
import drender.message.MessageConnectionNew;
import drender.message.MessageQueue;

/**
 * This thread listen to the specified port number waiting for incoming TCP
 * connections from the buyers interested to join the auction owned by the
 * seller consuming the message queue.
 */
public class ServerListener extends Thread
{
	private MessageQueue mq;
	private int listenPort;

	public ServerListener(int port, MessageQueue mq)
	{
		this.mq = mq;
		this.listenPort = port;
	}

	@Override
	public void run()
	{
		ServerSocket serverSock = null;
		try
		{
			serverSock = new ServerSocket();
			serverSock.bind(new InetSocketAddress(listenPort));
			while(true)
			{
//				System.out.println("Thread ServerListener listening");
				Socket sock = serverSock.accept();
				sock.setKeepAlive(true);
				Message msg = new MessageConnectionNew(sock, listenPort);
				mq.postMessage(msg);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		if(serverSock != null)
		{
			try
			{
				serverSock.close();
			}
			catch(IOException e)
			{
			}
		}
		System.out.println("Thread ServerListener terminated");
	}

	@Override
	public String toString()
	{
		return "ServerListener";
	}
}
