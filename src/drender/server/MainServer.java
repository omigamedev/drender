package drender.server;

import java.util.ArrayList;

import drender.message.Message;
import drender.message.MessageCommand;
import drender.message.MessageConnectionEnd;
import drender.message.MessageConnectionNew;
import drender.message.MessageQueue;
import drender.message.remote.MessageRemoteCommand;
import drender.message.remote.MessageRemoteString;
import drender.net.Peer;

public class MainServer
{
	public static final int listenPortDaemon = 1234;
	public static final int listenPortWorker = 1235;

	private MessageQueue mqin;
	private RenderQueue renderQueue;
	private ArrayList<Peer> peers;
	private ArrayList<Peer> workers;
	private ArrayList<Peer> daemons;
	private ServerListener listenerWorkers;
	private ServerListener listenerDaemons;
	private CommandListener command;
	
	public static void main(String[] args)
	{
		new MainServer().run();
	}

	public void run()
	{
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			@Override
			public void run()
			{
				System.out.println("Goodbye");
			}
		});
		
		peers = new ArrayList<Peer>();
		workers = new ArrayList<Peer>();
		daemons = new ArrayList<Peer>();
		mqin = new MessageQueue();
		renderQueue = new RenderQueue(workers);
		renderQueue.start();
		listenerDaemons = new ServerListener(listenPortDaemon, mqin);
		listenerDaemons.start();
		listenerWorkers = new ServerListener(listenPortWorker, mqin);
		listenerWorkers.start();
		command = new CommandListener(mqin);
		command.start();

		while(true)
		{
			Message m = mqin.getMessage();

			// if(m instanceof MessageRemote)
			// System.out.println("Server: " + m);

			if(m == null)
			{
				System.out.println("NULL message");
				continue;
			}

			switch(m.getType())
			{
			case Message.TYPE_COMMAND:
				MessageCommand mc = (MessageCommand) m;
				if(mc.getCommand().equals("render"))
				{
					if(workers.isEmpty())
					{
						System.out.println("No workers connected");
					}
					else
					{
						// lftp -e "glob -a rm images/*; bye" -u render,render -p 21 localhost
						String[] params = mc.getParams().trim().split(" ");
						if(params.length == 3)
						{
							renderQueue.setCancelQueue(false);
							int indexFrom = Integer.parseInt(params[0]);
							int indexTo = Integer.parseInt(params[1]);
							int totalFrames = indexTo - indexFrom;
							String filename = params[2];
							int slots = workers.size() * 4;
							float frames = ((float) totalFrames / (float)slots);
							float offset = 0;
							for(int i = 0; i < slots; i++)
							{
								int frameStart = (int) Math.floor(offset) + indexFrom;
								int frameEnd = (int) Math.floor(offset+frames) + indexFrom;
//								System.out.println("Job for " + p.getHost() + ": from " + frameStart + " to " + frameEnd);
//								p.sendMessage(new MessageRemoteRender(filename, frameStart, frameEnd, taskCount));
								renderQueue.enqueue(filename, frameStart, frameEnd);
								offset += frames;
							}
						}
						else
						{
							System.out.println("Usage: render [from] [to] [filename]");
						}
					}
				}
				else if(mc.getCommand().equals("daemon") || mc.getCommand().equals("worker"))
				{
					String[] params = mc.getParams().trim().split(" ");
					if(params.length > 1)
					{
						int id = Integer.parseInt(params[0]);
						String daemonCommand = params[1];
						String daemonParams = "";
						for(int i = 2; i < params.length; i++)
							daemonParams += " " + params[i];
						Peer peer = peers.get(id);
						if(peer != null)
						{
							if(mc.getCommand().equals("daemon"))
								if(peer.getPeerType() == Peer.TYPE_DAEMON)
									peer.sendMessage(new MessageRemoteCommand(daemonCommand, daemonParams));
								else
									System.out.println("Peer " + id + " is not a daemon");

							if(mc.getCommand().equals("worker"))
								if(peer.getPeerType() == Peer.TYPE_WORKER)
									peer.sendMessage(new MessageRemoteCommand(daemonCommand, daemonParams));
								else
									System.out.println("Peer " + id + " is not a worker");
						}
					}
					else
					{
						System.out.println("Usage: daemon|worker id command [params..]");
					}
				}
				else if(mc.getCommand().equals("daemons") || mc.getCommand().equals("workers"))
				{
					String[] params = mc.getParams().trim().split(" ");
					if(params.length > 0)
					{
						String nodeCommand = params[0];
						String nodeParams = "";
						for(int i = 1; i < params.length; i++)
							nodeParams += " " + params[i];
						for(Peer peer : (mc.getCommand().equals("daemons") ? daemons : workers))
							peer.sendMessage(new MessageRemoteCommand(nodeCommand, nodeParams));
					}
					else
					{
						System.out.println("Usage: daemons|workers command [params..]");
					}
				}
				else if(mc.getCommand().equals("status"))
				{
					System.out.println("Connected peers: " + peers.size());
					for(Peer pd : daemons)
					{
						System.out.println(" - daemon-" + peers.indexOf(pd) + ": " + pd.getHost());
						for(Peer pw : workers)
							if(pw.getHost().equals(pd.getHost()))
								System.out.println(" --- worker-" + peers.indexOf(pw));
					}
				}
				else if(mc.getCommand().equals("restart"))
				{
					for(Peer peer : daemons)
						peer.sendMessage(new MessageRemoteCommand("restart", ""));
				}
				else if(mc.getCommand().equals("update"))
				{
					for(Peer peer : daemons)
						peer.sendMessage(new MessageRemoteCommand("update", ""));
				}
				else if(mc.getCommand().equals("kill"))
				{
					renderQueue.setCancelQueue(true);
					for(Peer peer : daemons)
						peer.sendMessage(new MessageRemoteCommand("kill", ""));
					for(Peer peer : workers)
						peer.setPeerState(Peer.STATE_IDLE);
				}
				break;
			case Message.TYPE_CONNECTION_NEW:
				MessageConnectionNew mcn = (MessageConnectionNew) m;
				Peer peer;
				try
				{
					peer = new Peer(mcn.getSocket(), mqin);
					peer.start();

					peers.add(peer);

					if(mcn.getListenPort() == listenPortDaemon)
					{
						peer.setPeerType(Peer.TYPE_DAEMON);
						daemons.add(peer);
						System.out.println("Connected to daemon " + peer.getHost() + " -> " + peer.getIp());
					}
					else if(mcn.getListenPort() == listenPortWorker)
					{
						peer.setPeerType(Peer.TYPE_WORKER);
						workers.add(peer);
						System.out.println("Connected to worker " + peer.getHost() + " -> " + peer.getIp());
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				break;
			case Message.TYPE_CONNECTION_END:
				MessageConnectionEnd mce = (MessageConnectionEnd) m;
				peers.remove(mce.getPeer());
				workers.remove(mce.getPeer());
				daemons.remove(mce.getPeer());
				break;
			case Message.TYPE_REMOTE_TASKOUTPUT:
//				MessageRemoteTaskOutput mrto = (MessageRemoteTaskOutput) m;
//
//				if(StringUtils.isPrintable(mrto.getStreamText()))
//					System.out.println(mrto.getTaskID() + "-" + peers.indexOf(mrto.getPeer()) + ": " + mrto.getStreamText());
				
				break;
			case Message.TYPE_REMOTE_STRING:
				MessageRemoteString mrs = (MessageRemoteString)m;
				
				System.out.println(mrs);
				
				if(mrs.getString().equals("render-start"))
				{
					mrs.getPeer().setPeerState(Peer.STATE_RENDERING);
				}
				else if(mrs.getString().equals("render-complete"))
				{
					mrs.getPeer().setPeerState(Peer.STATE_IDLE);
					mrs.getPeer().stopTaskTimer();
					if(renderQueue.isEmpty())
						System.out.println("Rendering completed");
				}
				else if(mrs.getString().equals("render-fail"))
				{
					mrs.getPeer().incrementFailCount();
				}
				break;
			}
		}
	}
}
