package drender.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import drender.message.MessageCommand;
import drender.message.MessageQueue;

public class CommandListener extends Thread
{
	private MessageQueue mq;

	public CommandListener(MessageQueue mq)
	{
		this.mq = mq;
	}

	@Override
	public void run()
	{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String line;
		try
		{
//			System.out.print("Insert command: ");
			while((line = in.readLine()) != null)
			{
				line = line.trim();
				int idx = line.indexOf(" ");
				if(idx >= 0)
					mq.postMessage(new MessageCommand(line.substring(0, idx), line.substring(idx + 1)));
				else
					mq.postMessage(new MessageCommand(line, ""));
//				System.out.print("Insert command: ");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		super.run();
	}
}
