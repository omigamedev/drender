package drender.server;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

import drender.message.remote.MessageRemoteRender;
import drender.net.Peer;

public class RenderQueue extends Thread
{

	private boolean running;
	private LinkedBlockingQueue<RenderTask> queue;
	private ArrayList<Peer> workers;
	private int taskCount;
	private boolean cancelQueue;

	public RenderQueue(ArrayList<Peer> workers)
	{
		queue = new LinkedBlockingQueue<RenderTask>(1024);
		this.workers = workers;
	}

	public void enqueue(String fileName, int start, int end)
	{
		boolean result = queue.offer(new RenderTask(start, end, fileName));
		if(result == false)
		{
			System.out.println("Failed to enqueue " + fileName + " " + start + " " + end);
		}
	}

	@Override
	public void run()
	{
		setRunning(true);
		setCancelQueue(false);
		while(isRunning())
		{
			RenderTask rt;
			try
			{
				rt = queue.take();
			}
			catch(InterruptedException e1)
			{
				return;
			}
			boolean done = false;
			while(!done)
			{
				try
				{
					Thread.sleep(500);
				}
				catch(InterruptedException e)
				{
				}
				
				if(cancelQueue)
				{
					queue.clear();
					done = true;
					break;
				}
				
				Peer min = null;
				for(Peer p : workers)
				{
					boolean suitable = false;
					
					if(queue.isEmpty())
						suitable = p.getPeerState() != Peer.STATE_DISABLED;
					else
						suitable = p.getPeerState() == Peer.STATE_IDLE;
					
					if(suitable)
					{
						if(min != null)
						{
							int pieces = queue.size() + 1;
							long t_min = min.getTaskETA() + min.getTaskMeanTime() * pieces;
							long t_p = p.getTaskETA() + p.getTaskMeanTime();
							min = t_min < t_p ? min : p;
						}
						else
							min = p;
					}
				}
				if(min != null && min.getPeerState() == Peer.STATE_IDLE)
				{
					System.out.println("Job for " + min.getHost() + ": from " + rt.start + " to " + rt.end);
					min.sendMessage(new MessageRemoteRender(rt.fileName, rt.start, rt.end, taskCount));
					min.setPeerState(Peer.STATE_PENDING);
					min.startTaskTimer();
					done = true;
				}
				taskCount++;
			}
		}
	}
	
	public boolean isEmpty()
	{
		return queue.isEmpty();
	}

	public boolean isRunning()
	{
		return running;
	}

	public void setRunning(boolean running)
	{
		this.running = running;
	}

	public boolean isCancelQueue()
	{
		return cancelQueue;
	}

	public void setCancelQueue(boolean cancelQueue)
	{
		this.cancelQueue = cancelQueue;
	}
}

class RenderTask
{
	public int start;
	public int end;
	public String fileName;

	public RenderTask(int start, int end, String fileName)
	{
		super();
		this.start = start;
		this.end = end;
		this.fileName = fileName;
	}
}
